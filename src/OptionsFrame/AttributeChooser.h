#ifndef ATTRIBUTECHOOSER_H
#define ATTRIBUTECHOOSER_H

#include <CtrlLib/CtrlLib.h>
#include <CtrlCore/CtrlCore.h>

#include <string>
#include <vector>

using namespace Upp;

class AttributeChooser : public TreeCtrl {
  std::vector<Option*> _choices;
public:
  AttributeChooser(const std::vector<std::string>& choices);
  ~AttributeChooser();

  void SetOption(std::string name, bool value);
};


#include "../MWindow.h"

#endif
