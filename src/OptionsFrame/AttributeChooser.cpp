#include "AttributeChooser.h"



AttributeChooser::AttributeChooser(const std::vector<std::string>& choices) {
  this->NoRoot(true);
  this->RenderMultiRoot(true);

  for(std::string name : choices){
    Option* new_opt = new Option;
    new_opt->SetLabel(name.c_str());
    *new_opt << [this, new_opt, name](){
      ((MWindow*) ((Ctrl*) this)->GetParent()->GetParent())->DGraph().set_attribute_display(name, new_opt->Get());
    };

    this->_choices.push_back(new_opt);
    this->Add(0, TreeCtrl::Node(*new_opt));
  }
}


AttributeChooser::~AttributeChooser(){
  for(Option* opt : this->_choices){
    delete opt;
  }
}

void AttributeChooser::SetOption(std::string name, bool value){
  for(Option* opt : this->_choices){
    if(opt->GetLabel() == (String) name){
      if(opt->Get() != value){
        opt->PseudoPush();
      }
      break;
    }
  }
}
