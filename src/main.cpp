#include <CtrlLib/CtrlLib.h>
#include <CtrlCore/CtrlCore.h>

#include <iostream>

#include "MWindow.h"


GUI_APP_MAIN
{

  const Vector<String>& parameters = CommandLine();
  if(parameters.size() && parameters[0] == "--file"){
    MWindow app (parameters[1]);
    app.Run();
  }

}
