#ifndef MWINDOW_H
#define MWINDOW_H

#include <CtrlLib/CtrlLib.h>
#include <CtrlCore/CtrlCore.h>
#include <functional>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <chrono>

#include "GraphFrame/DisplayGraph.h"
#include "OptionsFrame/AttributeChooser.h"


class MWindow : public TopWindow {

  const int _settings_tree_width = 130;

  TreeCtrl _settings_tree;
  AttributeChooser _options_tree;


  DisplayGraph _dgraph;

  Button _archive_button;

public:
  MWindow(String path);

  virtual void Layout();

  virtual void MWindowLayout();

  DisplayGraph& DGraph(){
    return this->_dgraph;
  }
};

#endif
