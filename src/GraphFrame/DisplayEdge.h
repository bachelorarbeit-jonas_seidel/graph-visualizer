#ifndef DISPLAY_EDGE_H
#define DISPLAY_EDGE_H


#include <CtrlLib/CtrlLib.h>
#include <CtrlCore/CtrlCore.h>
#include <vector>
#include <set>
#include <string>
#include <cmath>

#include "Custom_Types/DraggableImageCtrl.h"
#include "DisplayNode.h"
#include "../../../../discrete_optimization_library/Graphtheory/Graphtheory.h"

using namespace Upp;

class DisplayNode;

class DisplayEdge : public DraggableImageCtrl {
  static std::vector<Image> arrow_body_precompute[];

  static Image* arrow[];

  static size_t edge_count;

  int local_arrow_length;
  int local_rotation;

  DisplayNode* _start;
  DisplayNode* _target;

  Edge* _repr_edge;

  ArrayCtrl _attributes;
  Display _column_display;
  EditDouble _dbl_editor;
public:
  DisplayEdge(Edge* repr_edge, DisplayNode* start, DisplayNode* target);

  ~DisplayEdge();

  void Precompute();

  void InsertArrow(Image& card, Point coord, int length, bool active);

  virtual void ParentChange();

  virtual void Dragged();

  void set_attribute_display(std::string attribute_name, bool value);
};

#endif
