#ifndef DISPLAYGRAPH_H
#define DISPLAYGRAPH_H

#include <CtrlLib/CtrlLib.h>
#include <CtrlCore/CtrlCore.h>
#include <iostream>
#include <fstream>
#include <map>
#include <stdexcept>

#include "../../../../discrete_optimization_library/Graphtheory/Graphtheory.h"


#include "DisplayEdge.h"
#include "DisplayNode.h"

using namespace Upp;

class DisplayGraph : public StaticRect {
  const int _boder_space = 50;

  Graph _n;
  std::set<Ctrl*> _draggable;
  std::set<Ctrl*> _drag;
  std::set<DisplayNode*> _nodes;
  std::set<DisplayEdge*> _edges;


  std::map<Node*, DisplayNode*> _display_lookup;


  const size_t _column_seperation = 200;
  const size_t _row_separation = 100;
public:

  virtual void ChildMouseEvent(Ctrl* child, int event, Point p, int zdelta, dword keyflags);

  virtual void MouseMove(Point p, dword flags);

  virtual void MouseLeave();

  virtual void LeftUp(Point p, dword keyflags);

  virtual void DisplayGraphLayout();

  void add_node(DisplayNode* node);

  void add_edge(DisplayEdge* edge);

  void scan_graph(std::ifstream& file);

  void set_attribute_display(std::string attribute_name, bool value);

  DisplayGraph(String path);

  DisplayGraph(DisplayGraph&) = delete;
  DisplayGraph(DisplayGraph&&) = delete;
  void operator=(DisplayGraph&) = delete;
  void operator=(DisplayGraph&&) = delete;

  ~DisplayGraph();

  Graph& GetGraph(){
    return this->_n;
  }
};

#include "../MWindow.h"

#endif
