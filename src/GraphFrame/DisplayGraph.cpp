#include "DisplayGraph.h"

void DisplayGraph::ChildMouseEvent(Ctrl* child, int event, Point p, int zdelta, dword keyflags){
  switch(event){
    case LEFTDOWN:
      {
        Ctrl* grouping = child;
        while (grouping->GetParent() != this) grouping = grouping->GetParent();
        if(this->_draggable.find(grouping) != this->_draggable.end()){
          this->_drag.insert(grouping);
        }
      }
      break;
    case LEFTUP:
      {
        Ctrl* grouping = child;
        while (grouping->GetParent() != this) grouping = grouping->GetParent();
        this->_drag.erase(grouping);
      }
      break;
    case MOUSEMOVE:
      Ctrl* grouping = child;
      while (grouping->GetParent() != this) grouping = grouping->GetParent();
      if(this->_drag.find(grouping) != this->_drag.end()){
        Point target_pos = {p.x-grouping->GetSize().cx/2, p.y-grouping->GetSize().cy/2};

        Logc x = grouping->GetPos().x;
        Logc y = grouping->GetPos().y;
        grouping->SetPos({
          {1, x.GetA() + target_pos.x, x.GetB()},
          {1, y.GetA() + target_pos.y, y.GetB()}
        });
        ((DraggableImageCtrl*) grouping)->Dragged();
      }
      break;
  }
}

void DisplayGraph::MouseMove(Point p, dword flags){
  for(Ctrl* child : this->_drag){
    child->SetPos({
      {1, p.x - child->GetSize().cx/2, child->GetPos().x.GetB()},
      {1, p.y - child->GetSize().cy/2, child->GetPos().y.GetB()}
    });
    ((DraggableImageCtrl*) child)->Dragged();
  }
}

void DisplayGraph::MouseLeave(){
  if(!HasMouseInFrame({{0,0},this->GetSize()})) {
    this->_drag.clear();
  }
}

void DisplayGraph::LeftUp(Point p, dword keyflags){
  this->_drag.clear();
}

void DisplayGraph::add_node(DisplayNode* node){
  this->Add(*node);
  this->_nodes.insert({node});
}

void DisplayGraph::add_edge(DisplayEdge* edge){
  this->Add(*edge);
  this->_edges.insert({edge});
}

void DisplayGraph::scan_graph(std::ifstream& file){
  this->_n = Graph(file);

  for(Node* node : this->_n.nodes()){
    DisplayNode* dn = new DisplayNode(node);
    this->add_node(dn);
    this->_display_lookup.insert({node, dn});
  }

  for(Edge* edge : this->_n.edges()){
    DisplayEdge* de = new DisplayEdge(edge, this->_display_lookup.find({edge->from()})->second, this->_display_lookup.find({edge->to()})->second);
    this->add_edge(de);
  }
}

void DisplayGraph::DisplayGraphLayout(){
  std::vector<Node*> starting_nodes = this->_n.select_sources();
  std::set<Node*> starting_nodes_searchable (starting_nodes.begin(), starting_nodes.end());

  this->_n.conditional_bfs_all_components(
    [](Node* from, Edge* via, bool used_in_traversal){},
    [this, &starting_nodes_searchable](Edge* via, Node* node) -> bool {
      if(via == nullptr){
        if(starting_nodes_searchable.count(node)){
          return true;
        }else{
          throw std::invalid_argument("graph either not weakly connected or not acyclic");
        }
      }
      DisplayNode* predecessor = this->_display_lookup.find(via->to(node))->second;
      DisplayNode* dnode = this->_display_lookup.find(node)->second;
      if(predecessor->column() > dnode->column() || dnode->column() == 0){
        if(dnode->critical_predecessor() != nullptr) dnode->critical_predecessor()->critical_successors().erase(dnode);

        dnode->critical_predecessor() = predecessor;
        predecessor->critical_successors().insert({dnode});

        dnode->column() = predecessor->column()+1;
        return true;
      }
      return false;
    },
    {starting_nodes.begin(), starting_nodes.end()},
    true
  );


  size_t offset = 0;
  size_t width = 0;
  for(Node* csnode : starting_nodes_searchable){
    DisplayNode* dnode = this->_display_lookup.find(csnode)->second;
    size_t tmp = dnode->spacer_calc();
    width = std::max(dnode->position_block({this->_boder_space, (int) (this->_boder_space + offset)}), width);
    offset += tmp;
  }

  this->SetPos(
    {1, 0, (int) width + this->_boder_space},
    {1, 0, (int) offset + 3*this->_boder_space}
  );

  ((MWindow*) this->GetParent())->MWindowLayout();
}

void DisplayGraph::set_attribute_display(std::string attribute_name, bool value){
  for(DisplayEdge* dedge : this->_edges){
    dedge->set_attribute_display(attribute_name, value);
  }
}

DisplayGraph::DisplayGraph(String path) {
  std::ifstream file (path.ToStd());

  this->scan_graph(file);

  for(Ctrl* c : this->_nodes) {
    this->_draggable.insert(c);
  }
}

DisplayGraph::~DisplayGraph() {
  for(DisplayNode* node : this->_nodes){
    delete node;
  }
  for(DisplayEdge* edge : this->_edges){
    delete edge;
  }
}
