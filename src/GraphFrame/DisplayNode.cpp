#include "DisplayNode.h"

DisplayNode::DisplayNode(Node* repr_node) : _repr_node(repr_node), _name(repr_node->description()), _critical_predecessor(nullptr) {
  this->_column = 0;
  this->IgnoreMouse(false);
  this->SetImage(Rescale(StreamRaster::LoadFileAny("Images/inactive_node.png"), {75,75} ));

  this->_first_filled = false;
  this->_second_filled = false;
  /*
  auto s = this->GetSize();
  this << ef1 << ef2;
  ef1.LeftPos(s.cx/4, s.cx/2).TopPos(s.cx/5,20);
  ef2.LeftPos(s.cx/4, s.cx/2).TopPos(4*s.cx/5-20,20);
  */
  this->LeftPos(-100, 75).TopPos(-100, 75);
  this->Add(this->_name);
  this->_name.LeftPos(this->GetSize().cx/2-4, 20).TopPos(10, 20);
  this->_name.IgnoreMouse(true);

  ef1.SetDarkThemeEnabled(false);
  ef2.SetDarkThemeEnabled(false);
}

DisplayNode::DisplayNode(Node* repr_node, Point p) : DisplayNode(repr_node) {
  this->LeftPos(p.x,75).TopPos(p.y,75);
}

void DisplayNode::Dragged(){
  for(DisplayEdge* e : this->_edges){
    e->Dragged();
  }
}

size_t DisplayNode::spacer_calc(){
  size_t nec_space = 0;
  for(DisplayNode* dnode : this->critical_successors()){
    nec_space += dnode->spacer_calc();
  }
  nec_space = std::max((size_t) 150, nec_space);
  this->_spacer = nec_space;
  return nec_space;
}

size_t DisplayNode::spacer(){
  return this->_spacer;
}

size_t DisplayNode::position_block(Point position){
  size_t offset = 0;
  size_t width = 0;
  if(this->critical_successors().size()){
    for(DisplayNode* dnode : this->critical_successors()){
      width = std::max(width, dnode->position_block({position.x+150, (int) (position.y + offset)}));
      offset += dnode->spacer();
    }
  }else{
    width = position.x+125;
  }

  this->LeftPos(position.x, this->GetSize().cx).TopPos(position.y + (this->spacer() - this->GetSize().cy)/2, this->GetSize().cy);

  this->Dragged();
  return width;
}

void DisplayNode::add_edge(DisplayEdge* m){
  this->_edges.push_back(m);
}

void DisplayNode::SetUpper(const char* test){
  ef1.SetText(test);
}

void DisplayNode::SetLower(const char* test){
  ef2.SetText(test);
}
