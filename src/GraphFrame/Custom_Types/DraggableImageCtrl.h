#ifndef DRAGGABLECTRL_H
#define DRAGGABLECTRL_H


#include <CtrlLib/CtrlLib.h>
#include <CtrlCore/CtrlCore.h>

using namespace Upp;

class DraggableImageCtrl : public ImageCtrl{
public:
  virtual void Dragged() = 0;
};

#endif
