#include "DisplayEdge.h"

double log_argument(Point r){
  struct frac_2dim_vect{
    double x;
    double y;
  } p = {(r.x/std::sqrt(r.x*p.x+r.y*r.y)), (-r.y/std::sqrt(r.x*r.x+r.y*r.y))};
  double s = std::asin(p.y);
  double c = std::acos(p.x);
  if(p.x >= 0){
    return s;
  }
  return M_PI-s;
}

//std::pair<std::vector<Image>, std::vector<Image>> DisplayEdge::arrow_body_precompute;
std::vector<Image> DisplayEdge::arrow_body_precompute[] = {{},{}};
Image* DisplayEdge::arrow[] = {new Image(std::move(StreamRaster::LoadFileAny("Images/arrow_tip.png"))), new Image(std::move(StreamRaster::LoadFileAny("Images/arrow_tip_active.png")))};
size_t DisplayEdge::edge_count;

DisplayEdge::DisplayEdge(Edge* repr_edge, DisplayNode* start, DisplayNode* target) : _repr_edge(repr_edge), _start(start), _target(target), _column_display(StdDisplay()) {
  this->IgnoreMouse(true);
  ++DisplayEdge::edge_count;

  start->add_edge(this);
  target->add_edge(this);

  this->Precompute();
  this->Dragged();

  this->_attributes.Header(false).HideSb(true);
  this->_attributes.LeftPos(0, 60).TopPos(0, this->_attributes.GetTotalCy());
  this->_attributes.AddColumn("var").SetDisplay(this->_column_display);
  this->_attributes.AddColumn("val").SetDisplay(this->_column_display).Edit(this->_dbl_editor);


  this->_attributes.ColumnWidths("17 20");

  //Add(this->_attributes);
}

void DisplayEdge::ParentChange(){
  if(this->GetParent() != nullptr)
    this->GetParent()->Add(this->_attributes);
}

DisplayEdge::~DisplayEdge(){
  if(DisplayEdge::edge_count == 1){
    delete DisplayEdge::arrow[0];
    delete DisplayEdge::arrow[1];
  }
  --DisplayEdge::edge_count;
}

void DisplayEdge::Precompute(){

  for(int t = false; t <= true; t++){
    if(DisplayEdge::arrow_body_precompute[t].size() > 0) return;
    if(t == false){
      DisplayEdge::arrow_body_precompute[t].push_back(StreamRaster::LoadFileAny("Images/10p_arrow_body.png"));
    }else{
      DisplayEdge::arrow_body_precompute[t].push_back(StreamRaster::LoadFileAny("Images/10p_arrow_body_active.png"));
    }

    for(int i = 1; i < 10; i++){
      Image next = CanvasSize(DisplayEdge::arrow_body_precompute[t][i-1], 2*DisplayEdge::arrow_body_precompute[t][i-1].GetSize().cx, DisplayEdge::arrow_body_precompute[t][i-1].GetSize().cy);
      Copy(next, {next.GetSize().cx/2, 0}, DisplayEdge::arrow_body_precompute[t][i-1], {{0,0}, DisplayEdge::arrow_body_precompute[t][i-1].GetSize()});
      DisplayEdge::arrow_body_precompute[t].push_back(std::move(next));
    }
  }
}

void DisplayEdge::InsertArrow(Image& card, Point coord, int length, bool active){
  length = (((double) 7)/2) * length;
  if(length < 38) length = 38;
  if(length > DisplayEdge::arrow[active]->GetSize().cx){
    Image* old_arrow = DisplayEdge::arrow[active];

    int value = 11;
    int index = 0;
    while ( value + old_arrow->GetSize().cx < length ){
      value = value * 2;
      index++;
      if(index > 9) break;
    }

    DisplayEdge::arrow[active] = new Image(std::move(CanvasSize(Image(), value + old_arrow->GetSize().cx, old_arrow->GetSize().cy)));
    Copy(*DisplayEdge::arrow[active], {0, DisplayEdge::arrow[active]->GetSize().cy/2 - DisplayEdge::arrow_body_precompute[active][index].GetSize().cy/2}, DisplayEdge::arrow_body_precompute[active][index], {{0,0}, DisplayEdge::arrow_body_precompute[active][index].GetSize()});
    Copy(*DisplayEdge::arrow[active], {value, 0}, *old_arrow, {{0,0}, old_arrow->GetSize()});

    delete old_arrow;
  }

  Image tmp = std::move(Copy(*DisplayEdge::arrow[active], {{DisplayEdge::arrow[active]->GetSize().cx-length, 0}, Size{length, DisplayEdge::arrow[active]->GetSize().cy}}));
  Copy(card, coord, Rescale(tmp, {(int) ((double) 2/7 * tmp.GetSize().cx), (int) ((double) 2/7 * tmp.GetSize().cy)}), {{0,0}, tmp.GetSize()});
}

void DisplayEdge::Dragged(){
  Point p1 = this->_start->GetRect().CenterPoint();
  Point p2 = this->_target->GetRect().CenterPoint();

  int length = std::sqrt((p2.x-p1.x)*(p2.x-p1.x) + (p2.y-p1.y)*(p2.y-p1.y));
  this->LeftPos((p2.x + p1.x - length)/2, length).TopPos((p2.y + p1.y - length)/2, length);
  if(length != 0){
    // this->_attributes.SetPos({
    //   {CENTER, 0, 60},
    //   {CENTER, 0, this->_attributes.GetSize().cy}
    // });
    this->_attributes.SetRect({{this->GetRect().CenterPoint().x - 30, this->GetRect().CenterPoint().y - (this->_attributes.GetTotalCy()+5)/2}, Size{60, this->_attributes.GetTotalCy()+5}});
  }

  int rotation = -1800*log_argument(p2 - p1)/M_PI;
  if( std::abs(length - this->local_arrow_length) > 2 || std::abs(rotation - this->local_rotation)*length > 1800){
    // immediately keep other threads from redrawing
    this->local_arrow_length = length;
    this->local_rotation = rotation;

    Image local_arrow = CanvasSize(Image(), length, length);

    int start = this->_start->GetSize().cx/2;
    int upper = length - this->_target->GetSize().cx/2;

    std::pair<bool, Attribute> selected = this->_repr_edge->attribute("Selected");
    bool arrow_active = selected.first ? selected.second.value() : false;

    this->InsertArrow(local_arrow, {start, length/2 - 69/7}, upper-start, arrow_active);


    this->SetImage(Rotate(local_arrow, rotation));
  }
}

void DisplayEdge::set_attribute_display(std::string attribute_name, bool value) {
  if(attribute_name.length() == 0) throw std::invalid_argument("attributes name has to have non-zero length");

  std::stringstream variable_symbol_ss;
  variable_symbol_ss << attribute_name.c_str()[0];
  String variable_symbol = variable_symbol_ss.str();


  int position = this->_attributes.Find(variable_symbol, 0);
  if(value == true){
    switch (position){
      case -1:
        // not yet present; inserting:
        {
          auto attribute_search = this->_repr_edge->attribute(attribute_name);
          if(attribute_search.first){
            this->_attributes.Add({variable_symbol, attribute_search.second.value()});
          }
        }
        break;
      default:
        // already present
        break;
    }
  }else{
    switch (position){
      case -1:
        // not present; nothing to be done
        break;
      default:
        // present; deleting:
        this->_attributes.Remove(position);
    }
  }

  this->Dragged();
}
