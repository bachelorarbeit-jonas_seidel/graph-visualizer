#ifndef DISPLAY_NODE_H
#define DISPLAY_NODE_H


#include <CtrlLib/CtrlLib.h>
#include <CtrlCore/CtrlCore.h>
#include <vector>
#include <cmath>

#include "Custom_Types/DraggableImageCtrl.h"
#include "DisplayEdge.h"
#include "../../../../discrete_optimization_library/Graphtheory/Graphtheory.h"

using namespace Upp;

class DisplayEdge;

class DisplayNode : public DraggableImageCtrl {
  EditField ef1;
  EditField ef2;

  class bpainter : public Ctrl {
    std::string _text;
  public:
    bpainter(std::string text) : _text(text){
      this->SetPos(
        {1,-100,100},
        {1,-100,50}
      );
    }
    virtual void Paint(Draw& w){
      w.DrawText(0,0,_text.c_str(), StdFont(), Color(0,0,0));
    }
  } _name;

  std::vector<DisplayEdge*> _edges;

  Node* _repr_node;
  size_t _column;

  std::set<DisplayNode*> _critical_sucessor_nodes;
  DisplayNode* _critical_predecessor;
  size_t _spacer;

  bool _first_filled;
  std::string _first;
  bool _second_filled;
  std::string _second;
public:
  DisplayNode(Node* repr_node);

  DisplayNode(Node* repr_node, Point p);

  size_t& column(){
    return this->_column;
  }

  std::set<DisplayNode*>& critical_successors(){
    return this->_critical_sucessor_nodes;
  }

  DisplayNode*& critical_predecessor(){
    return this->_critical_predecessor;
  }

  virtual void Dragged();

  size_t spacer_calc();

  size_t spacer();

  size_t position_block(Point position);

  void add_edge(DisplayEdge* m);

  void SetUpper(const char* test);

  void SetLower(const char* test);
};


#endif
