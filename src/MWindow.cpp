#include "MWindow.h"

// todo: scan available attributes in graph and create options based on that
MWindow::MWindow(String path) : _dgraph(path), _options_tree({"Selected", "Flow", "Upper"}) {
  std::stringstream title;
  title << "displaying " << path.ToStd();
  this->Title(String(title.str()));

  this->_settings_tree.NoRoot(true);
  this->_settings_tree.Add(0, TreeCtrl::Node(this->_options_tree).SetSize({100,100}));
  this->Add(this->_settings_tree);
  this->Add(this->_dgraph);

  this->_dgraph.DisplayGraphLayout();
  this->_options_tree.SetOption("Upper", true); // Todo save in external config file


  this->_archive_button.SetLabel("archive");
  this->_archive_button << [this](){
    auto tmp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::stringstream path;
    path << ".archive/graphs/" <<  std::put_time(std::localtime(&tmp), "%F %T") << ".netw";
    std::ofstream ofs(path.str());
    ofs << this->_dgraph.GetGraph();
    ofs.close();
  };
  this->Add(this->_archive_button);
  this->_archive_button.SetPos({
    {RIGHT, 20, 60},
    {TOP, 20, 20}
  });

  Sizeable();
}

void MWindow::Layout(){
  this->_settings_tree.SetRect({
    {0, 0},
    Size{this->_settings_tree_width, this->GetSize().cy}
  });

  this->_dgraph.SetRect({
    {this->_settings_tree_width, 0},
    Size{this->GetSize().cx - this->_settings_tree_width, this->GetSize().cy}
  });
}

void MWindow::MWindowLayout(){
  this->_settings_tree.SetRect({
    {0, 0},
    Size{this->_settings_tree_width, this->_dgraph.GetSize().cy}
  });

  this->_dgraph.SetRect({
    {this->_settings_tree_width, 0},
    this->_dgraph.GetSize()
  });

  // overall width & overall heights
  Point cp = this->GetRect().TopLeft();
  this->SetRect({
    {cp.x, cp.y},
    Size{this->_dgraph.GetSize().cx + this->_settings_tree.GetSize().cx, std::max(this->_dgraph.GetSize().cy, this->_settings_tree.GetSize().cy)}
  });
}
