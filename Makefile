
all = $(shell find -type f -wholename "*")

graph_display: $(all)
	../upp/umk ../upp/uppsrc,.. graph_display GCC -b +GUI,SHARED ../graph_display/

.phony: clean
clean:
	rm -f graph_display
